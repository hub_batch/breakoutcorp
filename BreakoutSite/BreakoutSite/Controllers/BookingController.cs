﻿using BreakoutSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutSite.Controllers
{
    public class BookingController : Controller
    {
        private string strResult;
        private int intResult;
        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(ProfileModel model, BookingModel book)
        {
            using (SqlConnection con = new SqlConnection("Server=breakout-dc-1\\SQLEXPRESS;Database=BreakOutDB;User ID=break;Password=password123"))
            {
                using (SqlCommand command = new SqlCommand("SELECT ID FROM UserPass_Tab WHERE (Username = @User)"))
                {
                    command.Connection = con;
                    con.Open();
                    command.Parameters.AddWithValue("@User", ProfileModel.DisplayName);
                    SqlDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        strResult = dr[0].ToString();
                        intResult = int.Parse(strResult);
                    }
                    con.Close();
                }
                using (SqlCommand update = new SqlCommand("INSERT INTO Booking_Tab(ID, Date, Place) VALUES(@ID, @DATE, @PLACE)"))
                {
                    update.Connection = con;
                    con.Open();
                    update.Parameters.AddWithValue("@ID", intResult);
                    update.Parameters.AddWithValue("@DATE", book.BookDate);
                    update.Parameters.AddWithValue("@PLACE", book.BookPlace);
                    ProfileModel.HasBooking = true;
                    update.ExecuteNonQuery();
                }
            }
            return View();
        }
    }
}