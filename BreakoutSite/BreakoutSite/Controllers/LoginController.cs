﻿using BreakoutSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutSite.Controllers
{
    public class LoginController : Controller
    {
        public string strResult;
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel user)
        {
            using (SqlConnection con = new SqlConnection("Server=breakout-dc-1\\SQLEXPRESS;Database=BreakOutDB;User ID=break;Password=password123"))
            {
                string userquery = "SELECT * FROM UserPass_Tab WHERE (Username = @Username) AND (Password = @Password)";
                userquery += " SELECT SCOPE_IDENTITY()";
                using (SqlCommand cmd = new SqlCommand(userquery))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", user.Username);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    ViewBag.Username = user.Username;
                    ViewBag.Pass = user.Password;
                    try
                    {
                        int UserExist = (int)cmd.ExecuteScalar();
                        try
                        {
                            using (SqlCommand command2 = new SqlCommand("UPDATE Profile_Tab SET LoggedIn = @LoggedIn WHERE (DisplayName = @Username)"))
                            {
                                command2.Connection = con;
                                command2.Parameters.AddWithValue("@LoggedIn", 1);
                                command2.Parameters.AddWithValue("@Username", user.Username);
                                ProfileModel.Password = user.Password;
                                ProfileModel.DisplayName = user.Username;
                                ViewBag.LoggedIn = true;
                                ProfileModel.LoggedIn = 1;
                                command2.ExecuteNonQuery();

                            }
                        }
                        catch (SqlException)
                        {

                        }
                    }
                    catch (NullReferenceException)
                    {
                        ViewBag.AlertText = "ERROR: User does not exist.";
                    }
                }
            }
            return View();
        }
    }
}