﻿using BreakoutSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutSite.Controllers
{
    public class LogoutController : Controller
    {
        // GET: Logout
        public ActionResult Index()
        {
            using (SqlConnection con = new SqlConnection("Server=breakout-dc-1\\SQLEXPRESS;Database=BreakOutDB;User ID=break;Password=password123"))
            {
                using (SqlCommand update = new SqlCommand("UPDATE Profile_Tab SET LoggedIn = @LoggedIn WHERE(DisplayName = @Username)"))
                {
                    update.Connection = con;
                    con.Open();
                    update.Parameters.AddWithValue("@LoggedIn", 0);
                    update.Parameters.AddWithValue("@Username", ProfileModel.DisplayName);
                    ViewBag.LoggedIn = false;
                    ProfileModel.LoggedIn = 0;
                    update.ExecuteNonQuery();
                }
            }

            return View();
        }
    }
}