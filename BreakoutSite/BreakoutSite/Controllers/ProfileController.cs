﻿using BreakoutSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutSite.Controllers
{
    public class ProfileController : Controller
    {
        private string strResult;
        private int intResult;
        // GET: Profile
        public ActionResult Index(ProfileModel user)
        {
            ViewBag.DisplayName = ProfileModel.DisplayName;
            //using (SqlConnection con = new SqlConnection("Server=breakout-dc-1\\SQLEXPRESS;Database=BreakOutDB;User ID=break;Password=password123"))
            //{
            //    using (SqlCommand command = new SqlCommand("SELECT ID FROM Booking_Tab WHERE (ID = @ID)"))
            //    {
            //        command.Connection = con;
            //        con.Open();
            //        command.Parameters.AddWithValue("@ID", ProfileModel.UserID);
            //        SqlDataReader dr = command.ExecuteReader();
            //        while (dr.Read())
            //        {
            //            strResult = dr[0].ToString();
            //            intResult = int.Parse(strResult);
            //        }
            //        con.Close();
            //    }
            //}
            
            if (ProfileModel.HasBooking == false)
            {
                ViewBag.HasBooking = false;
            }
            else
            {
                ViewBag.HasBooking = true;
            }
            return View();
        }
    }
}