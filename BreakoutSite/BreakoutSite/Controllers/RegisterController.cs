﻿using BreakoutSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutSite.Controllers
{
    public class RegisterController : Controller
    {
        private string strResult;
        private int intResult;
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel account)
        {
            using (SqlConnection con = new SqlConnection("Server=breakout-dc-1\\SQLEXPRESS;Database=BreakOutDB;User ID=break;Password=password123"))
            {
                string query = "INSERT INTO UserPass_Tab(Username, Email, Password) VALUES(@Username, @Email, @Password)";
                query += " SELECT SCOPE_IDENTITY()";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    //Establishes the connection defined in the first using statement.
                    cmd.Connection = con;
                    //Opens the connection.
                    con.Open();
                    //Adds the values asked for in the SQL code. When you use @something, that tells the SQL parser "hey, this is looking for a parameter".
                    //This is using the UserModel's values, which you define in your view.
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    cmd.Parameters.AddWithValue("@Email", account.Email);
                    //You should always put these in a try catch- unless you're testing to see WHAT SQL exception is thrown.
                    //If you know what will be thrown, then give it a try/catch.
                    try
                    {
                        cmd.ExecuteNonQuery();
                        try
                        {
                            //Grabs the ID from the User table
                            using (SqlCommand command = new SqlCommand("SELECT ID FROM UserPass_Tab WHERE (Username = @User) AND (Password = @Pass)"))
                            {
                                command.Connection = con;
                                command.Parameters.AddWithValue("@User", account.Username);
                                command.Parameters.AddWithValue("@Pass", account.Password);
                                SqlDataReader dr = command.ExecuteReader();
                                while (dr.Read())
                                {
                                    strResult = dr[0].ToString();
                                    intResult = int.Parse(strResult);
                                    ProfileModel.UserID = intResult;
                                }
                                con.Close();
                            }
                        }
                        catch (SqlException exception)
                        {
                            ViewBag.AlertText = "ERROR: " + exception.ToString();
                            ViewBag.NewUserCreated = false;
                        }
                        try
                        {
                            //Takes the Username, the ID, and the Email and places it into the profile table.
                            using (SqlCommand update = new SqlCommand("INSERT INTO Profile_Tab(ID, DisplayName, Email, LoggedIn) VALUES(@ID, @DisplayName, @Email, @LoggedIn)"))
                            {
                                
                                update.Connection = con;
                                con.Open();
                                update.Parameters.AddWithValue("@ID", intResult);
                                update.Parameters.AddWithValue("@DisplayName", account.Username);
                                update.Parameters.AddWithValue("@Email", account.Email);
                                update.Parameters.AddWithValue("@LoggedIn", 0);
                                update.ExecuteNonQuery();
                            }
                        }
                        catch (SqlException exception)
                        {
                            ViewBag.AlertText = "ERROR: " + exception.ToString();
                            ViewBag.NewUserCreated = false;
                        }
                        ViewBag.NewUserCreated = true;
                        ViewBag.SuccessText = "Success.";
                    }
                    catch (SqlException exception)
                    {
                        ViewBag.AlertText = "ERROR: " + exception.ToString();
                        ViewBag.NewUserCreated = false;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return View(account);
        }
    }
}
