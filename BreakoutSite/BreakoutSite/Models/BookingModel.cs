﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutSite.Models
{
    public class BookingModel
    {
        public string BookDate { get; set; }
        public string BookPlace { get; set; }
    }
}