﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutSite.Models
{
    public class ProfileModel
    {
        public static int UserID { get; set; }
        public static string Password { get; set; }
        public static string DisplayName { get; set; }
        public static int LoggedIn { get; set; }
        public static bool HasBooking { get; set; }
        public static string BookingInformation { get; set; }
    }
}